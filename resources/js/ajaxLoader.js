var loadingArea = sel('#loading-area');

function loadHTML(url) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);

    request.onload = function() {
        if (this.status >= 200 && this.status < 400) {
            loadingArea.innerHTML = this.responseText;
        } else {
            console.log(`Error loading: "${url}"`);
        }
    };

    request.send();
}

ready(function() {
    window.location.hash = '#home';
    if (loadingArea.innerHTML.length === 0) {
        loadHTML('resources/ajax/home.html');
    }

    window.addEventListener("hashchange", function() {
        loadHTML('resources/ajax/' + window.location.hash.substr(1) + '.html');
    }, false);
});