'use strict';

let gulp 		= require('gulp'),
	sass 		= require('gulp-sass'),
	watch 		= require('gulp-watch'),
	concat		= require('gulp-concat'),
	esUglify 	= require('uglify-es'),
	composer	= require('gulp-uglify/composer'),
	browserSync = require('browser-sync').create();

let inputSCSS 	= "resources/scss/**/*.scss",
	outputCSS 	= "dist/resources/css/",

	inputJS		= "resources/js/**/*.js",
	outputJS	= "dist/resources/js/",
	webDocs		= [
		"**/*.{html,htm,php}",
		"!node_modules/**/*.{html,htm,php}",
		"!dist/**/*.{html,htm,php}"
	];

let options = {};
var esMinify = composer(esUglify, console);


gulp.task('default', function() {
	browserSync.init({
		server: {
			baseDir	: "dist",
			index	: "index.html"
		}
	});

	gulp.watch(inputSCSS, gulp.series('sassCompile'));
	gulp.watch(inputJS, gulp.series('esUglify'))
		.on('change', browserSync.reload);
	gulp.watch(webDocs, gulp.series('docsDist'))
		.on('change', browserSync.reload);
	gulp.watch("resources/images/**/*", gulp.series('imgDist'))
		.on('change', browserSync.reload);
});

gulp.task('sassCompile', function() {
	return gulp.src(inputSCSS)
    	.pipe(sass.sync({ outputStyle: 'compressed' })
		.on('error', sass.logError))
    	.pipe(gulp.dest(outputCSS))
		.pipe(browserSync.stream());
});

gulp.task('esUglify', function() {
	return gulp.src(inputJS)
		.pipe(concat('script.js'))
		.pipe(esMinify(options))
		.pipe(gulp.dest(outputJS));
});

gulp.task('docsDist', function() {
	return gulp.src(webDocs)
		.pipe(gulp.dest('dist/'));
});

gulp.task('imgDist', function() {
	return gulp.src("resources/images/**/*")
		.pipe(gulp.dest('dist/resources/images/'));
});
