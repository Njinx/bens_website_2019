# Ben's Website 2018

### File Structure
```
- Source HTML Files
- resources/
	- scss/
		- Source SCSS
	- images/
	- js/
		- Source JS
	- node_modules/
	- dist/
		- resources/
			- css/
				- Compiled CSS
			- js/
				- Compiled JS
		HTML Files
- package.json
- gulpfile.js
```
### Dependencies
* nodejs
* gulp
* gulp-watch
* gulp-sass
* gulp-uglify
* gulp-concat
* es-uglify
* composer
* browser-sync
